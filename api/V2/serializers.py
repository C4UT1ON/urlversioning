from rest_framework import serializers
from django.utils import timezone
from api.models import Item, Location, User

from api.base import serializers as base


class V2_UserSerializer(base.UserSerializer):
    pass
class V2_ItemSerializer(base.ItemSerializer):
    pass

class V2_LocationSerializer(base.LocationSerializer):
    class Meta(base.LocationSerializer):
        model = Location
        fields = ('id', 'title', 'description', 'image', 'capacity')
        # exclude = ('title','description')

