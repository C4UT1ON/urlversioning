
from rest_framework import filters

from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from api.V2.serializers import V2_ItemSerializer, V2_LocationSerializer, V2_UserSerializer
from api.base import views as base
from api.models import Item, Location, User
# Create your views here.


class ItemViewSet(viewsets.ModelViewSet):
    queryset = Item.objects.all()
    serializer_class = V2_ItemSerializer

    @action(methods=['get'], detail=False)
    def booked(self, request):
        booked = self.get_queryset().filter(is_booked=False)
        serializer = self.get_serializer_class()(booked)
        return Response(serializer.data)

    @action(methods=['get'], detail=False)
    def newest(self, request):
        newest = self.get_queryset().order_by('updated_at').last()
        serializer = self.get_serializer_class()(newest)
        return Response(serializer.data)

    @action(methods=['get'], detail=False)
    def oldest(self, request):
        oldest = self.get_queryset().order_by('updated_at').first()
        serializer = self.get_serializer_class()(oldest)
        return Response(serializer.data)


class LocationViewSet(base.LocationViewSet):
    serializer_class = V2_LocationSerializer    
    # @action(methods=['get'], detail=False)
    # def ascending(self, request):
    #     newest = self.get_queryset().order_by('title').last()
    #     serializer = self.get_serializer_class()(newest)
    #     return Response(serializer.data)
    #
    # @action(methods=['get'], detail=False)
    # def descending(self, request):
    #     oldest = self.get_queryset().order_by('title').first()
    #     serializer = self.get_serializer_class()(oldest)
    #     return Response(serializer.data)