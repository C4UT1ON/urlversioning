from rest_framework import routers
from api.V2.views import ItemViewSet, LocationViewSet

router = routers.SimpleRouter()
router.register(r'locations', LocationViewSet)
router.register(r'items', ItemViewSet)

urlpatterns = router.urls
