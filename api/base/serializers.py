from rest_framework import serializers
from django.utils import timezone
from api.models import Item, Location, User

class UserSerializer(serializers.ModelSerializer):
    # item = ItemSerializer(source='item_set')
    class Meta:
        model = User
        exclude = ('password', 'date_joined', 'is_superuser', 'groups', 'user_permissions', 'is_active', 'last_login')


class ItemSerializer(serializers.ModelSerializer):
    # booking = BookingSerializer(many=True)
    class Meta:
        model = Item
        fields = '__all__'
        # fields = ( 'id','title','created_at', 'updated_at', 'is_booked')

    def validate_title(self, value):
        qs = Item.objects.filter(title__iexact=value)  # testing for data redundancy
        if self.instance:  # including object instance
            qs = qs.exclude(title=self.instance.title)
            # qs = qs.exclude(image=self.instance.image)
        if qs.exists():
            raise serializers.ValidationError("Title already in use")
        # if 'Item title' not in value: # controlling data unpit
        #     raise serializers.ValidationError("Title must have `Item title { number }` for testing")
        return value


class LocationSerializer(serializers.ModelSerializer):
    # item = ItemSerializer(source='item_set')
    class Meta:
        model = Location
        fields = ['id', 'title', 'description', 'image']

    def validate_title(self, value):  # testing for data redundancy
        qs = Location.objects.filter(title__iexact=value)
        if self.instance:  # including object instance
            qs = qs.exclude(title=self.instance.title)
        if qs.exists():
            raise serializers.ValidationError("Title already in use")
        return value
