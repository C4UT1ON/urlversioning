
from rest_framework import filters

from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from api.base.serializers import ItemSerializer, LocationSerializer, UserSerializer
from api.models import Item, Location, User
# Create your views here.


class ItemViewSet(viewsets.ModelViewSet):
    queryset = Item.objects.all()
    serializer_class = ItemSerializer

    @action(methods=['get'], detail=False)
    def booked(self, request):
        booked = self.get_queryset().filter(is_booked=False)
        serializer = self.get_serializer_class()(booked)
        return Response(serializer.data)

    @action(methods=['get'], detail=False)
    def newest(self, request):
        newest = self.get_queryset().order_by('updated_at').last()
        serializer = self.get_serializer_class()(newest)
        return Response(serializer.data)

    @action(methods=['get'], detail=False)
    def oldest(self, request):
        oldest = self.get_queryset().order_by('updated_at').first()
        serializer = self.get_serializer_class()(oldest)
        return Response(serializer.data)


class LocationViewSet(viewsets.ModelViewSet):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer

    # def list(self, request):
    #     print(self.queryset)
    #     serializer = LocationSerializer(self.queryset, many=True)
    #     return Response(serializer.data)

    # def retrieve(self, request, id):
    #     # print(self.queryset.get(id))
    #     # qs =
    #     serializer = LocationSerializer(Location.objects.get(id=id))
    #     return Response(serializer.data)

    # @action(methods=['get'], detail=False)
    # def ascending(self, request):
    #     newest = self.get_queryset().order_by('title').last()
    #     serializer = self.get_serializer_class()(newest)
    #     return Response(serializer.data)
    #
    # @action(methods=['get'], detail=False)
    # def descending(self, request):
    #     oldest = self.get_queryset().order_by('title').first()
    #     serializer = self.get_serializer_class()(oldest)
    #     return Response(serializer.data)