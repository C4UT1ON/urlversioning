from django.db import models

# Create your models here.

import datetime
import uuid
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    # pass
    image = models.ImageField(upload_to='user/', null=True, blank=True)


class Location(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=50)
    # booking     = models.ManyToManyField(Booking, blank=True, related_name='location_bookings')
    image = models.ImageField(upload_to='location/', null=True, blank=True)
    # is_booked = models.BooleanField(default=False, editable=False)
    capacity = models.IntegerField(null=True)

    class Meta:
        ordering = ['title']

    def __str__(self):
        return self.title


class Item(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=30)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    image = models.ImageField(upload_to='item/', null=True, blank=True)

    class Meta:
        ordering = ['title']

    # def get_absolute_url(self):
    #     return reverse("item:item-detail", kwargs={"id": self.id})

    def __str__(self):
        return self.title